<?php
	/*
		### 抓取医案并缓存到本地

	*/

	exit('1');

	ini_set('max_execution_time', '0');
	$domain = 'http://www.zysj.com.cn';

	//遍历医案分页
	$pageList = array();
	for($i=1;$i<=41;$i++){
		$url = 'http://www.zysj.com.cn/yianxinde/index'.$i.'.html';
		$pageList[] = $url;
	}
	_getYiAnList($pageList,$domain);

	//抓取医案URL列表
	function _getYiAnList($urlList,$domain){
		if(count($urlList) < 1){
			return;
		}
		$url = array_shift($urlList);
		echo $url.'<br>';
		$output = _curlGet($url);
		$pattern='/\/yianxinde\/[0-9]{1,6}.*.html/';
		preg_match_all($pattern,$output,$res);
		_getYiAnInfo($res[0],$domain);
		_getYiAnList($urlList,$domain);	//回调
	}

	//抓取医案内容
	function _getYiAnInfo($urlList,$domain){
		if(count($urlList) < 1){
			return;
		}
		$url = $domain.array_shift($urlList);
		$output = _curlGet($url);

		//提取标题
		$pattern='/<h1>(.*)<\/h1>/';
		preg_match_all($pattern,$output,$res);
		$title = $res[1][0];

		if($title){
			//提取内容
			$pattern='/span id=\"time\".*<\/p>(.*)<ol id=\"page-nav\">/sU';
			preg_match_all($pattern,$output,$res);
			$info = $res[1][0];
			$path = './BingLi/'.iconv('utf-8','gbk',$title);
			F($path,$info);
		}
		sleep(1);
		_getYiAnInfo($urlList,$domain);//回调
	}
















/*
	### 抓取指定页面内容
*/
function _curlGet($url){
	$curlobj = curl_init();
	curl_setopt($curlobj,CURLOPT_URL,$url);	//设置访问网页的URL
	curl_setopt($curlobj,CURLOPT_RETURNTRANSFER,true);	//执行后不直接打印出
	$output = curl_exec($curlobj);						//执行抓取并把结果保存在变量中
	curl_close($curlobj);							//关闭curl
	return $output;
}



function dump($s,$type=false){
	echo '<pre>';
		var_dump($s);
	echo '</pre>';
	if($type){
		exit;
	}
}





/*
	### 向指定文件写入内容(自动创建不存在目录)
	参一：$psth (string) 	要写入文件的路径
	参二：$content (string) 	要写入的内容
	示例：
*/
//function WriteFile($path,$content){
function F($path,$content=false){
	if(func_num_args() > 1){	//存储数据
		$dir = dirname($path);
		if(!is_dir($dir)){
			Mkdirs($dir);
		}
		file_put_contents($path,$content);
	}else{ 						//获取数据
		if(file_exists($path)){
			return file_get_contents($path);
		}
	}
}



/*
	### 递归创建目录
	### 参一：目录路径
*/
function Mkdirs($p){
	if(!is_dir($p)){
		Mkdirs(dirname($p),0777,true);
		mkdir($p);
	}
}
