<?php
	/*
		### 抓取中药并缓存到本地

	*/

	exit('1');

	ini_set('max_execution_time', '0');

	$url = 'http://www.zysj.com.cn/zhongyaocai/index.html';
	$domain = 'http://www.zysj.com.cn';

	//获取药物所有字母页的连接地址
	$output = _curlGet($url);
	$pattern='/\/zhongyaocai\/index__[1-9][0-9]{0,3}.html/';
	preg_match_all($pattern,$output,$res);

	//dump($res[0]);

	_getCharUrl($res[0],$domain);



	//获取指定字母页下指定中药页链接
	$url = $domain.'/zhongyaocai/index__1.html';
function _getCharUrl($urlList1,$domain){
	if(count($urlList1)<1){
		return;
	}
	$url = $domain.array_shift($urlList1);
	$output = _curlGet($url);
	$pattern='/\/zhongyaocai\/yaocai_.*\.html/';
	preg_match_all($pattern,$output,$res);
	_getOrCacheSpaceYaoContent($res[0],$domain);
	_getCharUrl($urlList1,$domain);
}














/*
	### 获取指定中药的 名，性味，归经，功能并缓存到本地文件
	示例：
		$url = 'http://www.zysj.com.cn/zhongyaocai/yaocai_a/anchundan.html';
		_getOrCacheSpaceYaoContent($url);
*/
function _getOrCacheSpaceYaoContent($urlList,$domain){
	if(count($urlList) < 1){
		return;
	}
	$url = $domain.array_shift($urlList);	//获取指定中药详情url
	echo $url.'<br>';
	$output = _curlGet($url);				//获取指定中药详情内容
	$yaoMing = _getYaoMing($output);
	$xingWei = _getXingWei($output);
	$guiJing = _getGuiJing($output);
	$gongNeng = _getGongNeng($output);
	if($yaoMing){
		$str = '';
		$str .= '药名：'.$yaoMing."\r\n\r\n";
		$str .= '性味：'.$xingWei."\r\n\r\n";
		$str .= '归经：'.$guiJing."\r\n\r\n";
		$str .= '功能：'.$gongNeng."\r\n\r\n";
		$path = './yao/'.iconv('utf-8','gbk',$yaoMing);
		F($path, $str);
		echo $yaoMing."：缓存成功！<br>";
	}
	sleep(1);
	// if($count > 5){
	// 	$count = 0;
	// 	return;
	// }
	_getOrCacheSpaceYaoContent($urlList,$domain);
}


//获取本地文件内容 
// $path = './yao/'.iconv('utf-8','gbk','阿魏');
// $a = file($path);
// dump($a);
// //echo htmlentities(F($path));













/*
	### 获取 药名
	示例：
		echo _getYaoMing($output);
	结果：
		阿魏
*/
function _getYaoMing($output){
	$pattern='/\<h1\>.*\<\/h1\>/';
	preg_match_all($pattern,$output,$yanMing);
	if($yanMing[0][0]){
		return ereg_replace('<h1>|<\/h1>','',$yanMing[0][0]);
	}
}






/*
	### 获取 功能主治
	示例：
		echo _getGongNeng($output);
	结果：
		消积，散痞，杀虫。用于肉食积滞，瘀血症瘕，腹中痞块，虫积腹痛。，症瘕痞块，肉积，心腹冷痛，疟疾，痢疾，，化症消积，截疟，胸腹胀满
*/
function _getGongNeng($output){
	$pattern='/功能主治\<\/span\>.*\<\/p\>/';
	preg_match_all($pattern,$output,$gongNeng);
	if(count($gongNeng[0]) > 0){
		foreach($gongNeng[0] as $k=>$v){
			if($k > 0){
				$str = ereg_replace('功能主治|\<\/span\>|\<\/p\>|主|治','',$v);
				$arrTmp = preg_split('/，|；|、|。/',$str);
				foreach($arrTmp as $v2){
					if(stripos($gongNeng[0][0],$v2) === false){
						$gongNeng[0][0] = $gongNeng[0][0].'，'.$v2;
					}
				}
			}
		}
		return ereg_replace('功能主治|\<\/span\>|\<\/p\>','',$gongNeng[0][0]);
	}
}



/*
	### 获取 归经
	示例：
		echo _getGuiJing($output);
	结果：
		脾、胃。肝
*/
function _getGuiJing($output){
	$pattern='/归经\<\/span\>.*\<\/p\>/';
	preg_match_all($pattern,$output,$guiJing);
	if(count($guiJing[0]) > 0){
		foreach($guiJing[0] as $k=>$v){
			if($k > 0){
				for($i=0;$i<strlen($v);$i++){
					$tmpStr = mb_substr($v,$i,1,'utf-8');
					if(stripos($guiJing[0][0],$tmpStr) === false){
						$guiJing[0][0] = $guiJing[0][0].$tmpStr;
					}
				}			
			}
		}
		return ereg_replace('归经|\<\/span\>|\<\/p\>|；|经|入|归','',$guiJing[0][0]);
	}
}







/*
	### 获取 性味
	示例：
		echo _getXingWei($output);
	结果：
		苦、辛，温。平无毒
*/
function _getXingWei($output){
	$pattern='/性味\<\/span\>.*\<\/p\>/';
	preg_match_all($pattern,$output,$xingWei);
	if(count($xingWei[0]) > 0){
		foreach($xingWei[0] as $k=>$v){
			if($k > 0){
				for($i=0;$i<strlen($v);$i++){
					$tmpStr = mb_substr($v,$i,1,'utf-8');
					if(stripos($xingWei[0][0],$tmpStr) === false){
						$xingWei[0][0] = $xingWei[0][0].$tmpStr;
					}
				}			
			}
		}
		return ereg_replace('性味|\<\/span\>|\<\/p\>|；|味|性','',$xingWei[0][0]);
	}
}




/*
	### 抓取指定页面内容
*/
function _curlGet($url){
	$curlobj = curl_init();
	curl_setopt($curlobj,CURLOPT_URL,$url);	//设置访问网页的URL
	curl_setopt($curlobj,CURLOPT_RETURNTRANSFER,true);	//执行后不直接打印出
	$output = curl_exec($curlobj);						//执行抓取并把结果保存在变量中
	curl_close($curlobj);							//关闭curl
	return $output;
}



function dump($s){
	echo '<pre>';
		var_dump($s);
	echo '</pre>';
}





/*
	### 向指定文件写入内容(自动创建不存在目录)
	参一：$psth (string) 	要写入文件的路径
	参二：$content (string) 	要写入的内容
	示例：
*/
//function WriteFile($path,$content){
function F($path,$content=false){
	if(func_num_args() > 1){	//存储数据
		$dir = dirname($path);
		if(!is_dir($dir)){
			Mkdirs($dir);
		}
		file_put_contents($path,$content);
	}else{ 						//获取数据
		if(file_exists($path)){
			return file_get_contents($path);
		}
	}
}



/*
	### 递归创建目录
	### 参一：目录路径
*/
function Mkdirs($p){
	if(!is_dir($p)){
		Mkdirs(dirname($p),0777,true);
		mkdir($p);
	}
}
