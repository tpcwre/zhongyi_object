<?php
	/*
		### 抓取方剂并缓存到本地

	*/


	exit('1');

	ini_set('max_execution_time', '0');

	$url = 'http://www.zysj.com.cn/zhongyaofang/index.html';
	$domain = 'http://www.zysj.com.cn';

	//获取字母页URL列表 
	$output = _curlGet($url);
	$pattern='/\/zhongyaofang\/index_[1-9][0-9]{0,3}.html/';
	preg_match_all($pattern,$output,$res);

	_getCharUrl($res[0],$domain);	//获取字母页内容



//获取方剂URL列表
function _getCharUrl($urlList1,$domain){
	if(count($urlList1)<1){
		return;
	}
    $url = $domain.array_shift($urlList1);
    if($url == 'http://www.zysj.com.cn/zhongyaofang/index_1.html'){
	   $url = $domain.array_shift($urlList1);
    }
	echo $url;
	$output = _curlGet($url);
	$pattern='/\/zhongyaofang\/yaofang_.*\.html/';
	preg_match_all($pattern,$output,$res);		//方剂页URL列表
	_getYaoFang($res[0],$domain);
	_getCharUrl($urlList1,$domain);
}


//获取各方剂详情
function _getYaoFang($urlList,$domain){
	if(count($urlList) < 1){
		return;
	}
	$url = $domain.array_shift($urlList);
	$output = _curlGet($url);
	_getInfoOrCache($output);
	_getYaoFang($urlList,$domain);
}



/*
	### 将方剂内容提取并缓存到本地文件
	示例：
		_getInfoOrCache($info);
*/
function _getInfoOrCache($info){
	$pattern="/<h2.*功能主治.*<\/p>|<h1.*功能主治.*<\/p>/sU";
	preg_match_all($pattern,$info,$yaoFang);
	foreach($yaoFang[0] as $v){
		$arr = array();
		//提取方剂名
		$pm="/>(.*)<\/h2>|>(.*)<\/h1>/";
		preg_match_all($pm,$v,$ming);
		if($ming[1][0] || $ming[2][0]){
			$name = $ming[1][0]?:$ming[2][0];
			$arr['ming'] = $name;

			//提取处方
			$pm="/处方<\/span>(.*)<\/p>/";
			preg_match_all($pm,$v,$fang);
			//dump($fang);
			if($fang[1][0]){
				$arr['fang'] = $fang[1][0];
			}

			//提取功能
			$pm="/功能主治<\/span>(.*)<\/p>/";
			preg_match_all($pm,$v,$gong);
			//dump($gong);
			if($gong[1][0]){
				$arr['gong'] = $gong[1][0];
			}
			$path = './fang/'.iconv('utf-8','gbk',$name);
			F($path,json_encode($arr));
			//echo $name.'：缓存成功！<br><br>';
			usleep(300000);
		}

	}
}



























/*
	### 抓取指定页面内容
*/
function _curlGet($url){
	$curlobj = curl_init();
	curl_setopt($curlobj,CURLOPT_URL,$url);	//设置访问网页的URL
	curl_setopt($curlobj,CURLOPT_RETURNTRANSFER,true);	//执行后不直接打印出
	$output = curl_exec($curlobj);						//执行抓取并把结果保存在变量中
	curl_close($curlobj);							//关闭curl
	return $output;
}



function dump($s,$type=false){
	echo '<pre>';
		var_dump($s);
	echo '</pre>';
	if($type){
		exit;
	}
}





/*
	### 向指定文件写入内容(自动创建不存在目录)
	参一：$psth (string) 	要写入文件的路径
	参二：$content (string) 	要写入的内容
	示例：
*/
//function WriteFile($path,$content){
function F($path,$content=false){
	if(func_num_args() > 1){	//存储数据
		$dir = dirname($path);
		if(!is_dir($dir)){
			Mkdirs($dir);
		}
		try{	
			file_put_contents($path,$content);
		}catch(Exception $e){
			echo $path.' is error！';
		}
	}else{ 						//获取数据
		if(file_exists($path)){
			return file_get_contents($path);
		}
	}
}



/*
	### 递归创建目录
	### 参一：目录路径
*/
function Mkdirs($p){
	if(!is_dir($p)){
		Mkdirs(dirname($p),0777,true);
		mkdir($p);
	}
}
