<?php
	header("Content-Type: text/html;charset=utf-8"); 
	require('./public/fun.php');
	require('./public/sql.class.php');

	session_start();



	$type = $_REQUEST['type'];
	$tmp_arr = array();


	//需要验证token的项
	$token_v_arr = array(
		'editYao',
	);
	if(in_array($type, $token_v_arr)){
		$token = I('token','post');
		if(!VerifyToken1($_SESSION['user'],$token)){
		//	exit('10001');	//token非法
		}
	}



	//echo $type;
	switch($type){
		//添加病例
		case 'addBing':
			$name = I('name','post');
			$info = I('info','post');
			if(!$name || !$info){
				$tmp_arr['stat'] = 'err1';	//至少需要药名和内容
				exit(json_encode($tmp_arr));
			}
			$dataPath = './data/bing';
			$data = F($dataPath);
			$data2 = json_decode($data,1);
			for($i=0;$i<count($data2);$i++){
				$tmpName = trim($data2[$i]['name']);
				if($tmpName == $name){
					$tmp_arr['stat'] = 'err2';	//病例已存在
					exit(json_encode($tmp_arr));
				}
			}
			$tmp_yao = array(
				'name'=>$name,
				'info'=>$info,
			);
			array_unshift($data2,$tmp_yao);

			F($dataPath,json_encode($data2));
			$tmp_arr['stat'] = 'ok';
			$tmp_arr['token'] = SetToken1($_SESSION['user']);
			exit(json_encode($tmp_arr));
		break;


		//删除中药
		case 'deleteFang':
			$name = I('name','post');
			if(!$name){
				$tmp_arr['stat'] = 'err1';	//至少需要药名和功能
				exit(json_encode($tmp_arr));
			}
			$dataPath = './data/bing';
			$data = F($dataPath);
			$data2 = json_decode($data,1);
			$data3 = array();
			for($i=0;$i<count($data2);$i++){
				$tmpName = trim($data2[$i]['name']);
				if($tmpName != $name){
					$data3[] = $data2[$i];
				}
	
			}
			F($dataPath,json_encode($data3));
			$tmp_arr['stat'] = 'ok';
			$tmp_arr['token'] = SetToken1($_SESSION['user']);
			exit(json_encode($tmp_arr));
		break;





		//删除中药
		case 'deleteYao':
			$name = I('name','post');
			if(!$name){
				$tmp_arr['stat'] = 'err1';	//至少需要药名和功能
				exit(json_encode($tmp_arr));
			}
			$dataPath = './data/yao';
			$data = F($dataPath);
			$data2 = json_decode($data,1);
			$data3 = array();
			for($i=0;$i<count($data2);$i++){
				$tmpName = trim($data2[$i]['name']);
				if($tmpName != $name){
					$data3[] = $data2[$i];
				}
	
			}
			F($dataPath,json_encode($data3));
			$tmp_arr['stat'] = 'ok';
			$tmp_arr['token'] = SetToken1($_SESSION['user']);
			exit(json_encode($tmp_arr));
		break;


		//编辑中药
		case 'editYao':
			$name = I('name','post');
			$xing = I('xing','post');
			$jing = I('jing','post');
			$gong = I('gong','post');
			if(!$name || !$gong){
				$tmp_arr['stat'] = 'err1';	//至少需要药名和功能
				exit(json_encode($tmp_arr));
			}
			$dataPath = './data/yao';
			$data = F($dataPath);
			$data2 = json_decode($data,1);

			$v_index = -1;
			for($i=0;$i<count($data2);$i++){
				$tmpName = trim($data2[$i]['name']);
				if($tmpName == $name){
					$v_index = $i;
				}
	
			}
			//修改
			if($v_index >= 0){
				$data2[$v_index]['xing'] = $xing;
				$data2[$v_index]['jing'] = $jing;
				$data2[$v_index]['gong'] = $gong;
			}else{
				//添加
				$tmp_yao = array(
					'name'=>$name,
					'xing'=>$xing,
					'jing'=>$jing,
					'gong'=>$gong,
				);
				array_unshift($data2,$tmp_yao);
			}
			F($dataPath,json_encode($data2));
			$tmp_arr['stat'] = 'ok';
			$tmp_arr['token'] = SetToken1($_SESSION['user']);
			exit(json_encode($tmp_arr));
		break;


		//用户退出
		case 'exit':
			$_SESSION['user'] = '';
			exit('ok');
		break;


		//用户登陆
		case 'login':
			$user = I('user','post');
			$pwd = I('pwd','post');
			_checkVal($user,$pwd);
			if($user == 'a' && $pwd == 'b'){
				$tmp_arr['token'] = SetToken1($user);
				$tmp_arr['stat'] = 'ok';
				$_SESSION['user'] = $user;
			}else{
				$tmp_arr['stat'] = 'err';
			}
			exit(json_encode($tmp_arr));
		break;


		//获取方剂列表(文件数据库方式)
		case '_getBingList':
			$name = I('name','post');
			$info = I('info','post');
			$pNow = intval(I('pNow','post'));
			
			$cacheName = I('cacheName','post');

			$limit = 10;
			$start = ($pNow-1) * $limit;

			$res = F('./data/bing');
			$tmp_arr = array();	//装载结果的临时容器
			if(preg_match('/^\[{.*}\]$/',$res)){
				$arr = json_decode($res,1);
				foreach($arr as $k=>$v){
					$stat = true;
					if($name){
						if(!preg_match("/".$name."/",$v['name'])){
							$stat = false;
						}
					}
				
					if($info){
						$infos = explode(' ', $info);
						foreach($infos as $v2){
							if(!preg_match("/".$v2."/",$v['info'])){
								$stat = false;
							}
						}
					}

					//将符合条件的数据装载到临时容器
					if($stat){
						$tmp_arr[] = $v;
					}
				}

				//筛选分页数据
				$tmp_arr2 = array();
				for($i=0;$i<count($tmp_arr);$i++){
					if($i>=$start && $i<($start+$limit)){
						$tmp_arr2[] = $tmp_arr[$i];
					}
				}
				echo $res_json = json_encode($tmp_arr2);
				//F('./cache/'.$cacheName,$res_json);
			}
		break;





		//获取方剂列表(文件数据库方式)
		case '_getFangList':
			$name = I('name','post');
			$fang = I('fang','post');
			$gong = I('gong','post');
			$pNow = intval(I('pNow','post'));
			
			$cacheName = I('cacheName','post');

			$limit = 10;
			$start = ($pNow-1) * $limit;

			$res = F('./data/fang');
			$tmp_arr = array();	//装载结果的临时容器
			if(preg_match('/^\[{.*}\]$/',$res)){
				$arr = json_decode($res,1);
				foreach($arr as $k=>$v){
					$stat = true;
					if($name){
						if(!preg_match("/".$name."/",$v['name'])){
							$stat = false;
						}
					}
				
					if($fang){
						$fangs = explode(' ', $fang);
						foreach($fangs as $v2){
							if(!preg_match("/".$v2."/",$v['fang'])){
								$stat = false;
							}
						}
					}

					if($gong){
						$gongs = explode(' ', $gong);
						foreach($gongs as $v2){
							if(!preg_match("/".$v2."/",$v['gong'])){
								$stat = false;
							}
						}
					}

					//将符合条件的数据装载到临时容器
					if($stat){
						$tmp_arr[] = $v;
					}
				}

				//筛选分页数据
				$tmp_arr2 = array();
				for($i=0;$i<count($tmp_arr);$i++){
					if($i>=$start && $i<($start+$limit)){
						$tmp_arr2[] = $tmp_arr[$i];
					}
				}
				echo $res_json = json_encode($tmp_arr2);
				//F('./cache/'.$cacheName,$res_json);
			}
		break;







		//获取中药列表(文件数据库方式)
		case '_getYaoList':
			$xing = I('xing','post');
			$wei = I('wei','post');
			$jing = I('jing','post');
			$gong = I('gong','post');
			$name = I('name','post');

			$cacheName = I('cacheName','post');

			$pNow = intval(I('pNow','post'));

			$limit = 10;
			$start = ($pNow-1) * $limit;

			$res = F('./data/yao');
			$tmp_arr = array();	//装载结果的临时容器
			if(preg_match('/^\[{.*}\]$/',$res)){
				$arr = json_decode($res,1);
				foreach($arr as $k=>$v){
					$stat = true;
					if($xing){
						if(!preg_match("/".$xing."/",$v['xing'])){
							$stat = false;
						}
					}

					if($wei){
						if(!preg_match("/".$wei."/",$v['xing'])){
							$stat = false;
						}
					}

					if($jing){
						if(!preg_match("/".$jing."/",$v['jing'])){
							$stat = false;
						}
					}

					if($gong){
						$gongs = explode(' ', $gong);
						foreach($gongs as $v2){
							if(!preg_match("/".$v2."/",$v['gong'])){
								$stat = false;
							}
						}
					}

					if($name){
						$names = explode(' ', $name);

		
						if(count($names) == 1){		//单药查询使用正则匹配
							if(!preg_match("/".$name."/",$v['name'])){
								$stat = false;
							}
						}else{	//多药查询全名匹配
							$stat2 = array();
							foreach($names as $v2){
								//if(preg_match("/".$v2."/",$v['name'])){
								if(trim($v2) == trim($v['name'])){
									$stat2[] = 1;
								}
							}
							if(count($stat2) < 1){
								$stat = false;
							}
						}

					}
					//将符合条件的数据装载到临时容器
					if($stat){
						$tmp_arr[] = $v;
					}
				}

				//筛选分页数据
				$tmp_arr2 = array();
				for($i=0;$i<count($tmp_arr);$i++){
					if($i>=$start && $i<($start+$limit)){
						$tmp_arr2[] = $tmp_arr[$i];
					}
				}
				echo $res_json = json_encode($tmp_arr2);
				//F('./cache/'.$cacheName,$res_json);
			}
		break;


		//获取中药列表(mysql数据库方式)
		case '_getYaoList2':
			$xing = I('xing','post');
			$wei = I('wei','post');
			$jing = I('jing','post');
			$gong = I('gong','post');
			$name = I('name','post');
			$pNow = intval(I('pNow','post'));
			if($xing || $wei || $jing || $gong || $name){
				$where = ' where';
				$arr = array();
				if($xing){
					$where .= " xing like ?";
					$arr[] = "%{$xing}%";
				}
				if($name){
					$name_arr = explode(' ',$name);
					//dump($name_arr,1);
					if(count($arr) > 0){
						foreach($name_arr as $v){
							$where .= " and name like ?";
							$arr[] = "%{$v}%";
						}
					}else{
						foreach($name_arr as $k=>$v){
							if($k < 1){
								$where .= " name like ?";
								$arr[] = "%{$v}%";
							}else{
								$where .= " and name like ?";
								$arr[] = "%{$v}%";
							}
						}
					}
				}
				if($gong){
					$gong_arr = explode(' ',$gong);
					if(count($arr) > 0){
						foreach($gong_arr as $v){
							$where .= " and gong like ?";
							$arr[] = "%{$v}%";
						}
					}else{
						foreach($gong_arr as $k=>$v){
							if($k < 1){
								$where .= " gong like ?";
								$arr[] = "%{$v}%";
							}else{
								$where .= " and gong like ?";
								$arr[] = "%{$v}%";
							}
						}
					}
				}
				if($wei){
					if(count($arr) > 0){
						$where .= " and xing like ?";
						$arr[] = "%{$wei}%";
					}else{
						$where .= " xing like ?";
						$arr[] = "%{$xing}%";
					}
				}
				if($jing){
					if(count($arr) > 0){
						$where .= " and jing like ?";
						$arr[] = "%{$jing}%";
					}else{
						$where .= " jing like ?";
						$arr[] = "%{$jing}%";
					}
				}
			}else{
				$where = '';
			}
			$pdo = MyPdo::init();

			$limit = 5;
			$start = ($pNow -1) * $limit;

			$sql = "select name,xing,jing,gong from yao {$where} limit {$start},{$limit}";
			$res = $pdo->dql($sql,$arr);
			if($res){
				$arr['data'] = $res;
			}else{
				$arr['data'] = array();
			}
			echo json_encode($arr);
		break;



		default:
			echo 'default';
	}