<?php





/*
	### 使用用户名,ip,时间戳组合成token1并返回
	参一：用户名或openid
	参二：前后台类型，默认false后台，true 为前台
	return: 返回生成的token1
*/
function SetToken1($user){
	$ip = @$_SERVER["REMOTE_ADDR"];
	$token1 = md5($user.$ip.microtime());
	S('Token/'.$user,$token1);
	return $token1;
}


/*
	### 验证token1
	参一：用户名或openid
	参二：要验证的token1
	参三：前后台类型，默认false后台，true 为前台
	return: 返回生成的token1
*/
function VerifyToken1($user,$token1){
	$token2 = S('Token/'.$user);
	//S('ttttt',$token2."\r".$token1);
	if($token2 == $token1){
		return true;	//验证成功返回新的token1
	}else{
		return false;
	}
}



/*
	### 接收POST,GET,REQUEST数据，并处理特殊字符
	参一：接收名
	参二：默认空为get方式，get|post|all (request)
	return：返回将特殊字符处理后的数据
	示例：
		$number = I('number','post');
*/
function I($name,$type=''){
	switch($type){
		case 'p':$val = @$_POST[$name];break;
		case 'g':$val = @$_GET[$name];break;
		default:$val = @$_REQUEST[$name];break;
	}
	return trim($val);
}







/*
	### 检测变量
	示例：
		_checkVal($name,$pwd,,,);
*/
function _checkVal(){
	global $tmp_arr;
	$args = func_get_args();
	$stat = true;
	foreach($args as $k => $v){
		if(!$v){
			$tmp_arr['stat'] = 'err1';
			exit(json_encode($tmp_arr));
		}
	}
}






	

	


/*
	### 验证字段值是否合法
	参一：正则匹配模式
	参二：要验证的变量
	参三：非法后的提示内容或回调函数名
	示例：
		Field_Check(/.{3,}/,StoreMain_Name,'商户名太短!');
		或：
		Field_Check(/.{3,}/,StoreMain_Name,function(){
			Void_Token('商户名太短');
		});

	注：匹配中文时需使用 /php/u  u 表示使用unicode(utf-8)匹配方式 
		Field_Check('/^.{3,20}$|^[\x{4e00}-\x{9fa5}]{3,20}$/u',$name,'商户名长度为3-20个字符!');
*/
function Field_Check($p,$val){
	if(!preg_match($p,$val)){
		return false;
	}
	return true;
}



















//打印信息
function dump($data,$stat=false){
	echo '<pre>';
		var_dump($data);
	echo '</pre>';
	if($stat){
		exit;
	}
}







/*
	###  获取指定日期的年月日当天零时的时间戳
		参一：$tag 获取哪天的的标记 如：要获取昨天的'-1 day'

		示例：
			echo $a = GetDateYmd('-1 day');		//1525968000
			echo date('Y-m-d H:i:s',$a);		//2018-05-11 00:00:00

*/
function GetDateYmd($tag){
	return strtotime(date('Y-m-d',strtotime($tag,time())));
}
















/*
	### 检测并替换字串中的特殊字符为实体字符 (' , " , > , < , / , \)
	### 参一：源字串 
	### 参二：是否反解析，默认false为特殊字符转实体
	### 参三：不参与替换的字符,默认为空
	### return : 返回替换后的字串
	### 示例：
		$b = ChangeChar($a);	//转实体
		$val2 = ChangeChar($val,1); //实体转字符
*/
function ChangeChar($val,$stat=false,$sign=''){
	if($stat){
		$p = '/&#60/';
		$val = preg_replace($p,"<",$val);
		$p = '/&#62/';
		$val = preg_replace($p,">",$val);
		$p = '/&#39/';
		$val = preg_replace($p,"'",$val);
		$p = '/&#34/';
		$val = preg_replace($p,'"',$val);
		if($sign != '/'){
			$p = '/&#47/';
			$val = preg_replace($p,"/",$val);
		}
		$p = '/&#92/';
		$val = preg_replace($p,"\\",$val);
		$p = '/&#59/';
		$val = preg_replace($p,";",$val);

		$p = '/&#40/';
		$val = preg_replace($p,"(",$val);

		$p = '/&#41/';
		$val = preg_replace($p,")",$val);

		//$p = '/&nbsp/';
		//$val = preg_replace($p," ",$val);
		return $val;
	}else{
		$p ='/</';
		$val = preg_replace($p,"&#60",$val);
		$p ='/>/';
		$val = preg_replace($p,"&#62",$val);
		$p ="/'/";
		$val = preg_replace($p,"&#39",$val);
		$p ='/"/';
		$val = preg_replace($p,"&#34",$val);
		if($sign != '/'){
			$p = '/\//';
			$val = preg_replace($p,"&#47",$val);
		}
		$p = '/\\\/';
		$val = preg_replace($p,"&#92",$val);
		$p = '/;/';
		$val = preg_replace($p,"&#59",$val);

		$p = '/\(/';
		$val = preg_replace($p,"&#40",$val);
		$p = '/\)/';
		$val = preg_replace($p,"&#41",$val);
		
		//$p = '/\s/';
		//$val = preg_replace($p,"&nbsp",$val);
		return $val;
	}
}





/*
	### 永久缓存数据
	参一：缓存路径 (string)
	参二：缓存的值（string）
	return：
		当只有参一时是获取缓存并返回空或对应的值
		当有两个参数时是设置缓存，无返回值
	示例：
		S('a','aaa');	//设置
		echo S('a');	//获取
*/
function S($p,$v=false){
	if($v === ''){
		if(is_file($p)){
			unlink($p);
		}
		return;
	}
	if($v !== false){
		if(!is_dir(dirname($p))){
			Mkdirs(dirname($p));
		}
		file_put_contents($p,$v);
	}else{
		if(!file_exists($p)){
			return;
		}
		return file_get_contents($p);
	}
}



/*
	### 向指定文件写入内容(自动创建不存在目录)
	参一：$psth (string) 	要写入文件的路径
	参二：$content (string) 	要写入的内容
	示例：
*/
//function WriteFile($path,$content){
function F($path,$content=false){
	if(!$path){
		return;
	}
	if(func_num_args() > 1){	//存储数据
		$dir = dirname($path);
		if(!is_dir($dir)){
			Mkdirs($dir);
		}
		file_put_contents($path,$content);
	}else{ 						//获取数据
		if(file_exists($path)){
			return file_get_contents($path);
		}
	}
}





/*
	### 递归创建目录
	### 参一：目录路径
*/
function Mkdirs($p){
	if(!is_dir($p)){
		Mkdirs(dirname($p),0777,true);
		mkdir($p);
	}
}




//递归删除目录及文件
function DelDirs($p){
	if(is_file($p)){
		unlink($p);
	}
	if(is_dir($p)){
		$op = opendir($p);
		while($rd = readdir($op)){
			if($rd == '.' || $rd == '..'){
				continue;
			}
			$p2 = $p.'/'.$rd;
			DelDirs($p2);
		}
		rmdir($p);
		closedir($op);
	}
}




