<?php
class MyPdo{
	private static $obj = null;
	private $p = '';
	private $host = 'localhost';
	private $db = 'zhongyi';
	private $root = 'root';
	private $pwd = 'xiaodong123';



	//public static function init($host='',$db='',$root='',$pwd='',$stat=false){
	public static function init(){
		if(MyPdo::$obj == null){
			self::$obj = new MyPdo();
		}
		return self::$obj;
	}


	private function __construct(){

		if($this->p == ''){
			try{
				$this->p = new PDO("mysql:host={$this->host};dbname={$this->db}","{$this->root}","$this->pwd");
				$this->p->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
				$this->p->query("set names utf8");
				return $this->p;
			}catch(PDOException $e){
				die('数据库连接失败'.$e->getMessage());
			}
		}
	}




	/*
		@查询操作
		@parm 	$sql (string)	sql语句
		@parm 	$arr (Array)	索引数组形式的参数	
		@parm 	$type 	string	查询单条或多条，默认false查询多条,true为查询单条
		@return
	*/
	public function dql($sql,$arr,$type=false){
		//记录sql语句
		$this->SaveSql($sql,$arr);

		$st = $this->p->prepare($sql);
		$st->execute($arr);
		if(!$type){
			$res = $st->fetchAll(PDO::FETCH_ASSOC);
		}else{
			$res = $st->fetch(PDO::FETCH_ASSOC);
		}
		if($res){
			return $res;
		}else{
			return false;
		}
	}




	/*
		@增删改操作
		@parm 	$sql (string)	sql语句
		@parm 	$arr (Array)	索引数组形式的参数
		@return  
				添加操作成功时返回最后添加的id
				删除操作成功时返回受影响的行数
				修改操作成功时返回受影响的行数
				操作失败时返回false
	*/
	public function dml($sql,$arr){
		$this->SaveSql($sql,$arr);
		$st = $this->p->prepare($sql);
		$st->execute($arr);
		if($this->p->lastInsertId() > 0){
			return $this->p->lastInsertId();
		}else if($st->rowCount() > 0){
			return $st->rowCount();
		}else{
			return false;
		}
	}



	/*
		### 记录SQL语句
		参一：pdo方式的sql语句
		参二：pdo的数组参数
		参三：SQL语句保存的路径
	*/
	private function SaveSql($sql,$arr,$path=''){
		$sql_str_arr = explode('?',$sql);
		$sql3 = '';
		foreach ($sql_str_arr as $k=>$v) {
			if(isset($arr[$k])){
				$sql3 .= $v.$arr[$k];
			}else{
				$sql3 .= $v.';';
			}
		}
		$p = '/(%.*%)/';
		$sql4 = preg_replace($p,"'$1'",$sql3);
/*		$p = '/goods_title = (.*) union/';
		$sql4 = preg_replace($p,"goods_title = '$1' union",$sql4);*/

		$p = '/=\s?(\S*)\s{1}/';
		if(preg_match($p,$sql3)){
			$sql4 = preg_replace($p,"= '$1' ",$sql4);
		}

		$res2 = '';
		$path2 = '';
		if($path){
			if(file_exists($path)){
				$res = file_get_contents($path);
				$res2 = $sql."\r\n".$sql4."\r\n\r\n".$res;
			}else{
				$res2 = $sql4;
			}
			$path2 = $path;
		}else{
			$path2 = 'z_sql_statment.txt';
			if(file_exists($path2)){
				$res = file_get_contents($path2);
				$res2 = $sql."\r\n".$sql4."\r\n\r\n".$res;
			}else{
				$res2 = $sql."\r\n".$sql4;
			}
		}
		
		$size = 1024*10;
		if(strlen($res2) > $size){
			$res2 = substr($res2,0,$size);
		}
		file_put_contents($path2,$res2);
	}
	
}













