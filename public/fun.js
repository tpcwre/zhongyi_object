//退出登陆
function exit(){
	if(!confirm('确认退出？')){
		return;
	}
	sessionStorage.user = '';
	sessionStorage.token = '';
	var data = {
		type:'exit'
	}
	After(data,function(res){
		location.reload();
	});
}

//显示登陆用户及退出
function _checkUser(){
	if(sessionStorage.user){
		$('.userConArea').show();
	}else{
		$('.userConArea').hide();
	}
}










/*
	### 为关键字加粗着色
	参一：原字串
	参二：关键字  string | array
	参三：指定颜色

*/
function _keyBold(str,keyword,color){
	var p = eval('/'+keyword+'/g');
	//var tmp = ChangeChar(str);
	var tmp = str;
	if(color){
		if(typeof(keyword) == 'object' && keyword.length > 1){
			for(var i=0;i<keyword.length;i++){
				var p = eval('/'+keyword[i]+'/g');
				tmp = tmp.replace(p,"<span style='padding:5px;color:"+color+";font:normal bold 17px normal'>"+keyword[i]+"</span>");
			}
			return tmp;
		}else{
			//return ChangeChar(str).replace(p,"<span style='padding:5px;color:"+color+";font:normal bold 17px normal'>"+keyword+"</span>");
			return tmp.replace(p,"<span style='padding:5px;color:"+color+";font:normal bold 17px normal'>"+keyword+"</span>");
		}
	}else{
		if(typeof(keyword) == 'object' && keyword.length > 1){
			for(var i=0;i<keyword.length;i++){
				var p = eval('/'+keyword[i]+'/g');
				tmp = tmp.replace(p,"<span style='padding:5px;color:blue;font:normal bold 17px normal'>"+keyword[i]+"</span>");
			}
			return tmp;
		}else{
			//return ChangeChar(str).replace(p,"<span style='padding:5px;color:blue;font:normal bold 17px normal'>"+keyword+"</span>");
			return tmp.replace(p,"<span style='padding:5px;color:blue;font:normal bold 17px normal'>"+keyword+"</span>");
		}
	}
}






function _parseRes(res){
	if(/^\[{.*}\]$/.test(res)){
		return JSON.parse(res);
	}else{
		return '';
	}
}


/*
	### 检测JSON数据格式必需是{...}
	参二：type 
			false 默认，后台管理使用
			true : 商户管理使用

*/
function _checkJsonFormat(res,type){
	if(!/^{.*}$/.test(res)){
		A('返回数据异常！');
		return;
	}
	var tmp = JSON.parse(res);
	if(tmp.token){
		sessionStorage.token = tmp.token;
	}
	return tmp;
}
















/*
	###   ajax 异步加载 (JQUERY方式)
	参一：要传送的数据
	参二：成功回调
	参三：请求的后台地址.默认无值为前端后台地址，>0 为后台地址，有值且非int为当前参数地址
	参四：是否异步.当无值时默认为异步，当有值时为同步
	参五：提交方式 默认为post,有值为get
	参六：过期时间，默认为3秒
	示例：
		var data = {
					token:t,
					type:type,
					openid:openid
				};

		 Ajax(data,function(res){
		 	alert(res);
		 });
		 或
		 },1);
*/
function Ajax(data,fun,url,async,stat,timeout){
	if(!stat){
		stat = 'post';
	}else{
		stat = 'get';
	}
	async = async?false:true;
	if(!url){
		A('缺少URL');exit
	}
	if(!timeout){
		var timeout = 1000;
	}
	var ajaxBody = $.ajax({
		url:url,
		type:stat,
		async:async,
		data:data,
		timeout:timeout,
		success:function(rq_res,rq_stat,rq_xhr){
			fun(rq_res,rq_stat,rq_xhr);
		},
		error:function(){
			//ajaxBody.abort();
			fun('false');
		}
	});
}


//前台请求数据专用
function Ajax2(data,fun){
	Ajax(data,function(res){
	 	fun(res);
	},'./interface.php',1);
}


//后台操作数据专用
function After(data,fun){
	data.token = sessionStorage.token;
	Ajax(data,function(res){
	 	fun(res);
	},'./interface.php',1);
}






/*
	### 检测字段
	示例：
		Field_Check(/^.{3,20}$/,StoreMainEdit_Name,'js商户名长度为3-20个字符!');
*/
function Field_Check(p,str,notice){
	if(!p.test(str)){
		A(notice);exits
	}
}





/*
	### 检测并替换字串中的特殊字符为实体字符 (' , " , > , < , / , \)
	### 参一：源字串 
	### 参二：是否反解析,及分割特殊字符与数值
				空 为特殊字符转实体  默认
				1  为实体转字符
				1+ 将实体字符与数值的分割。如 &#392143 会产生乱码，分割为 &#39 2143
	### return : 返回替换后的字串
	### 示例：
		var content = $('textarea[name=content]').val().trim();
		content = ChangeChar(content);
		或
		ChangeChar(obj.info_title,1)
*/
function ChangeChar(val,stat){
	if(stat){
		if(stat > 1){
			if(/&#[0-9]{2,}/.test(val)){
				var p2 = /([^&#]*)(&#[0-9]{2})([0-9].*)/g;
				var val =  val.replace(p2,"$1$2 $3");
			}
			return val;
		}

		var p2 = /&#60/g;
		val = val.replace(p2,"<");
		var p2 = /&#62/g;
		val = val.replace(p2,">");
		var p2 = /&#39/g;
		val = val.replace(p2,"'");
		var p2 = /&#34/g;
		val = val.replace(p2,'"');
		var p2 = /&#47/g;
		val = val.replace(p2,"\/");
		var p2 = /&#92/g;
		val = val.replace(p2,"\\");
		var p2 = /&#59/g;
		val = val.replace(p2,";");
		var p2 = /&#40/g;
		val = val.replace(p2,"(");
		var p2 = /&#41/g;
		val = val.replace(p2,")");
		//var p2 = /&nbsp/g;
		//val = val.replace(p2," ");
		return val;
	}else{
		var p2 = /</g;
		val = val.replace(p2,"&#60");
		var p2 = />/g;
		val = val.replace(p2,"&#62");
		var p2 = /\'/g;
		val = val.replace(p2,"&#39");
		var p2 = /\"/g;
		val = val.replace(p2,"&#34");
		var p2 = /\//g;
		val = val.replace(p2,"&#47");
		var p2 = /\\/g;
		val = val.replace(p2,"&#92");
		var p2 = /;/g;
		val = val.replace(p2,"&#59");
		var p2 = /\(/g;
		val = val.replace(p2,"&#40");
		var p2 = /\)/g;
		val = val.replace(p2,"&#41");
		//var p2 = / /g;
		//val = val.replace(p2,"&nbsp");
		return val;
	}
}





/*
	### 检测确认
	示例：
		if(!_confirm()){
			return;
		}
*/
function _confirm(){
	if(!confirm('确认该操作？')){
		return false;
	}
	var vcode = Math.round(Math.random()*10000);
	if(vcode != prompt('请输入确认码：'+vcode)){
	        A('确认码不正确！');
	        return false;
	}
	return true;
}











/*
    ### 无限分页最新(无总页，效率分页*推荐*)
        参一：当前页
        参二：允许显示最大页码
        参三：成功的回调函数
        参四：>> 后翻的最大页码 (对应后台 $max_page)
        参五：当前页前后各显示的页数
		参六：指定要填充的元素id
        需要一个id为 fpage 的容器元素 如 <div style="text-align:center;margin:20px" id="fpage"> </div>
        示例：

		
			function _init(){
				_getFangList(function(res){
					var tmp = _parseRes(res);
					if(tmp.length < 1){
						pMax = pNow-1;
						Fpage(pMax,pMax,function(p){
							pNow = p;
							pMax = p+1;
							_init();
						},1,1,'#fpage')
						return;
					}else{
						Fpage(pNow,pMax,function(p){
							pNow = p;
							pMax = p+1;
							_init();
						},1,1,'#fpage')
					}	
					.......
				});
			}
	
*/
function Fpage(PageNo,max_page,fun,step,loop,ele) {
	if(!loop){
		loop=2;
	}
	if(!step){
		step=6;
	}
	PageNo = parseInt(PageNo);
    var start = PageNo - loop;
    if (start < 1) {
        start = 1;
    }
    var end = PageNo + loop;
    if(end > max_page){
    	end = max_page;
    }
    var fpage_str = '';
	if(PageNo > (loop+1)){
		fpage_str += "<a style='margin:5px;border:1px solid;padding:3px 5px;color:#aaa' PageNo='1' >首页</a>";
	}
    for (var i = start; i <= end; i++) {

        if (PageNo == i) {
            fpage_str += "<a style='margin:5px;border:1px solid blue;padding:3px 5px;color:#fff;background:blue' PageNo=" + i + ">" + i + "</a>";
        } else {
            fpage_str += "<a style='margin:5px;border:1px solid;padding:3px 5px;color:#aaa' PageNo=" + i + ">" + i + "</a>";
        }
    }
    var next = PageNo + step;
    if(next >= max_page){
    	next = max_page
    }
    //console.log(max_page);
	if(PageNo < max_page){
		fpage_str += "<a style='margin:5px;border:1px solid;padding:3px 5px;color:#aaa' PageNo="+next+" >>></a>";
	}
	if(!ele){
		ele = '#fpage';
	}
	$(ele).html(fpage_str);
    $(ele+" a").click(function () {
    	if(PageNo == $(this).attr('PageNo')){
    		return;
    	}
        PageNo = $(this).attr('PageNo');
        fun(PageNo);
    });
}











/*
	### 将时间戳转换成 年月日格式的字串 
	### 参一：stamp js时间戳 
		参二：stat无值反回年月日，有值返回年月日时分秒,有值且为2时返回时分秒
	### return : 返回年月日格式的字串
	### 示例：
		var t2 = StampToYmd(server_time);	//2018-5-15
*/
function StampToYmd(stamp,stat){
	var t = new Date(stamp);
	//计算年月日
	var t2 = t.getFullYear()+"-"+(t.getMonth()>=9?t.getMonth()+1:'0'+(t.getMonth()+1))+"-"+(t.getDate()>=10?t.getDate():'0'+t.getDate());
	if(stat){
		//计算时分秒
		var t3 = (t.getHours()>=10?t.getHours():'0'+t.getHours())+':'+(t.getMinutes()>=10?t.getMinutes():'0'+t.getMinutes())+':'+(t.getMinutes()>=10?t.getMinutes():'0'+t.getMinutes());
		if(stat == 2){
			return t3;	//返回时分秒
		}
		return  t2+' '+t3;	//返回年月，时分秒
	}else{
		return t2;	//返回年月日
	}
}





/*
	### 蒙板提示框(提示时页面被盖不可操作)
	### 参一：提示内容
	### 需要：函数需要一个 id='notice' 的元素做容器
	### 示例：
		 <div id='Notice'></div>
		 Notice('信息发布成功！'); //开启提示框
		 Notice();	//关闭提示框
*/
var Notice_deg = 1;
var Notice_rotate;
function Notice(text){
	if(text){
		var str = "<div class='sca_Notice' style='z-index:100;position:fixed;top:0px;width:100%;height:100%;background:rgba(255,100,100,0.5)'><div id='notice_text' style='background:#fff;width:200px;margin:20% auto;text-align:center;padding:30px;border-radius:10px'><div class='sca_Notice_img' style='padding:10px;border-top:1px solid #bbb;border-right:1px solid #999;border-bottom:1px solid #666;display:inline-block;border-radius:25px'></div><div>"+text+"</div></div></div>";
		$('body').append(str);
		Notice_rotate = setInterval(function(){
			$(".sca_Notice_img").css({'transform':'rotate('+Notice_deg+'deg)'});
			Notice_deg +=25;
		},100)
	}else{
		//$('.sca_Notice').html('');
		$('.sca_Notice').remove();
		clearInterval(Notice_rotate);
	}
}



function C(res){
	console.log(res);
}



function A(res){
	alert(res);
}




















